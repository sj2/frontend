/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ProductComponent } from '../product.component';
import { ProductListComponent } from './product-list.component';

describe('ProductListComponent', () => {

  let fixture: any;
  let debugElement : any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductComponent,
        ProductListComponent
      ],
    });
    TestBed.compileComponents();

    fixture = TestBed.createComponent(ProductListComponent);
    debugElement = fixture.debugElement;
  });

  it("should render a list of product components", async(() => {
    // TODO:Inject some products in the fixture component
    expect(debugElement.nativeElement.querySelector(".products")).toBeTruthy();
  }));

});
