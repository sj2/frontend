import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [ProductService]
})
export class ProductListComponent implements OnInit, OnDestroy {
  private products : Array<any>;
  private productsSubscription;

  constructor(private productService : ProductService) {

  }

  ngOnInit() {
    this.productsSubscription = this.productService.getProducts()
    .subscribe(
      products => this.products = products,
      error => this.handleError(error)
    );
  }

  ngOnDestroy() {
    this.productsSubscription && this.productsSubscription.unsubscribe();
  }

  handleError(error : Error): void {
    // TODO Show some error message
  }
}
