/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ProductComponent } from './product.component';

describe('ProductComponent', () => {

  let fixture: ProductComponent;
  let debugElement : any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductComponent
      ],
    });
    TestBed.compileComponents();

    fixture = TestBed.createComponent(ProductComponent);
    debugElement = fixture.debugElement;
  });

  it("should just render", async(() => {
    expect(debugElement.nativeElement.querySelector("h2")).toBeTruthy();
  }));

});
