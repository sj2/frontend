import { Observable } from "rxjs";
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class ProductService {
  private productsUrl = "/api/products";

  constructor(private http : Http) {

  }

  getProducts(): Observable<Array<any>> {
    return this.http.get(this.productsUrl).map(r => r.json() as Array<any>);
  }
}
